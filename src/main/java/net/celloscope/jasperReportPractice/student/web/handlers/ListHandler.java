package net.celloscope.jasperReportPractice.student.web.handlers;

import lombok.extern.slf4j.Slf4j;
import net.celloscope.jasperReportPractice.common.handler.BaseHandler;
import net.celloscope.jasperReportPractice.common.service.BaseService;
import net.celloscope.jasperReportPractice.student.helpers.dataclass.requests.AdmitStudentListRequestDTO;
import net.celloscope.jasperReportPractice.student.helpers.enums.QueryParam;
import net.celloscope.jasperReportPractice.student.services.ListService;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Component
@Slf4j
public class ListHandler extends BaseHandler {
    private final ListService service;
    public ListHandler(ListService service) {
        super(service);
        this.service = service;
    }

    private AdmitStudentListRequestDTO buildAdmitStudentListRequest(ServerRequest request) {
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return AdmitStudentListRequestDTO.builder()
                .fromDate(LocalDate.parse(request.queryParam(QueryParam.FROM_DATE.getValue()).orElse(LocalDate.now().toString()), dtf))
                .toDate(LocalDate.parse(request.queryParam(QueryParam.TO_DATE.getValue()).orElse(LocalDate.now().toString()), dtf))
                .reportType(request.queryParam(QueryParam.REPORT_TYPE.getValue()).toString())
                .build();
    }

    private AdmitStudentListRequestDTO buildStudentRollRequest(ServerRequest request) {
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return AdmitStudentListRequestDTO.builder()
                .fromDate(LocalDate.parse(request.queryParam(QueryParam.FROM_DATE.getValue()).orElse(LocalDate.now().toString()), dtf))
                .toDate(LocalDate.parse(request.queryParam(QueryParam.TO_DATE.getValue()).orElse(LocalDate.now().toString()), dtf))
                .reportType(request.queryParam(QueryParam.REPORT_TYPE.getValue()).toString())
                .rollNo(Integer.parseInt(request.queryParam(QueryParam.ROLL_NO.getValue()).get()))
                .build();
    }

    private AdmitStudentListRequestDTO buildStudentBranchRequest(ServerRequest request) {
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return AdmitStudentListRequestDTO.builder()
                .branch(request.queryParam(QueryParam.BRANCH_NAME.getValue()).orElse(""))
                .build();
    }


    public Mono<ServerResponse> admitStudentList(ServerRequest request){
        AdmitStudentListRequestDTO admitStudentListRequestDTO = buildAdmitStudentListRequest(request);
        return generateReportResponse(admitStudentListRequestDTO,"Admitted-Students-List", admitStudentListRequestDTO.getReportType(), service::setAdmittedStudentsListParameters);
    }

    public Mono<ServerResponse> findStudentByRoll(ServerRequest request){
        AdmitStudentListRequestDTO admitStudentListRequestDTO = buildStudentRollRequest(request);
        return generateReportResponse(admitStudentListRequestDTO,"Admitted-Students-List", admitStudentListRequestDTO.getReportType(), service::setStudentRollParameters);
    }

    public Mono<ServerResponse> findStudentByBranch(ServerRequest request) {
        AdmitStudentListRequestDTO admitStudentListRequestDTO = buildStudentBranchRequest(request);
        return generateReportResponse(admitStudentListRequestDTO,"Admitted-Students-List", admitStudentListRequestDTO.getReportType(), service::setStudentBranchParameters);
    }


}
