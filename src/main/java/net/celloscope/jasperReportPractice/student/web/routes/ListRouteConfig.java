package net.celloscope.jasperReportPractice.student.web.routes;

import lombok.extern.slf4j.Slf4j;
import net.celloscope.jasperReportPractice.student.web.handlers.ListHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.*;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;


@Slf4j
@Configuration
public class ListRouteConfig {

    @Bean
    public RouterFunction<ServerResponse> ListsRoutes(ListHandler listHandler) {
        return route()
                .before(this::logRequest)
                .path(RouteNames.STUDENT_LIST_BASE_URL,
                        builder -> builder
                                .nest(RequestPredicates.accept(APPLICATION_JSON), nestBuilder ->
                                        nestBuilder.GET(RouteNames.ADMITTED_STUDENT_LIST, listHandler::admitStudentList)
                                )
                                .nest(RequestPredicates.accept(APPLICATION_JSON), nestBuilder ->
                                        nestBuilder.GET(RouteNames.FIND_STUDENT_ROLL, listHandler::findStudentByRoll))
                                .nest(RequestPredicates.accept(APPLICATION_JSON), nestBuilder ->
                                        nestBuilder.GET(RouteNames.FIND_STUDENT_BY_BRANCH, listHandler::findStudentByBranch))
                ).after((req, res) -> logResponse(res))
                .build();
    }

    private ServerResponse logResponse(ServerResponse response) {
        log.info("Response sent: {}", response.statusCode());
        return response;
    }

    private ServerRequest logRequest(ServerRequest serverRequest) {
        log.info("Request Received From: {}", serverRequest.uri());
        return serverRequest;
    }
}
