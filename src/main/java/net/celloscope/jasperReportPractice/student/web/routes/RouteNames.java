package net.celloscope.jasperReportPractice.student.web.routes;

public class RouteNames {
    public static String STUDENT_LIST_BASE_URL = "/admit/v1/list";
    public static String ADMITTED_STUDENT_LIST = "/admitted";
    public static String FIND_STUDENT_ROLL = "/findbyroll";
    public static String FIND_STUDENT_BY_BRANCH = "/findbybranch";
}
