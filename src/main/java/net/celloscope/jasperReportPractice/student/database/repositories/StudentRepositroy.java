package net.celloscope.jasperReportPractice.student.database.repositories;

import lombok.ToString;
import net.celloscope.jasperReportPractice.student.database.entities.StudentEntity;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.core.support.AbstractEntityInformation;
import org.springframework.data.repository.query.Param;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


public interface StudentRepositroy extends ReactiveCrudRepository<StudentEntity, Integer> {

    @Query("SELECT rollno, firstname, lastname, branch, \"result\", joining_date FROM nda.student WHERE joining_date between :fromDate and :toDate")
    Flux<StudentEntity> admitedStudentsListRepo(@Param("fromDate") String fromDate, @Param("toDate") String toDate);

//    @Override
//    Mono<Boolean> existsById(Integer integer);
    @Query("SELECT rollno, firstname, lastname, branch, \"result\", joining_date\n" +
            "FROM nda.student\n" +
            "WHERE rollno = :rollno;\n")
    Flux<StudentEntity> statusByRollNo(@Param(("rollno")) int rollno);


//    @Override
    Flux<StudentEntity> findStudentEntityByBranch(String branch);
    Flux<StudentEntity> findAllByBranch(String branch);
}
