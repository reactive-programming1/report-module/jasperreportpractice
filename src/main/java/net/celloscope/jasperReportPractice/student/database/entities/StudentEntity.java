package net.celloscope.jasperReportPractice.student.database.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.data.annotation.Id;
import org.springframework.data.relational.core.mapping.Table;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name="student")
public class StudentEntity {
    @Id
    private int rollno;
    private String firstname;
    private String lastname;
    private String branch;
    private String result;
    private String joining_date;
}
