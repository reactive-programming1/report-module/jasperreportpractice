package net.celloscope.jasperReportPractice.student.services;

import lombok.extern.slf4j.Slf4j;
import net.celloscope.jasperReportPractice.common.dataclass.dtos.ServiceRequestDTO;
import net.celloscope.jasperReportPractice.common.service.BaseService;
import net.celloscope.jasperReportPractice.student.database.entities.StudentEntity;
import net.celloscope.jasperReportPractice.student.database.repositories.StudentRepositroy;
import net.celloscope.jasperReportPractice.student.helpers.dataclass.requests.AdmitStudentListRequestDTO;
import net.celloscope.jasperReportPractice.student.helpers.dataclass.responses.AdmitStudentListResponseDTO;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;


@Slf4j
@Service
public class ListService extends BaseService {
    private final StudentRepositroy studentRepositroy;

    public ListService(ResourceLoader resourceLoader, StudentRepositroy studentRepositroy) {
        super(resourceLoader);
        this.studentRepositroy = studentRepositroy;
    }
    public Flux<AdmitStudentListResponseDTO> successfulAdmitStudentList(AdmitStudentListRequestDTO requestDTO) {

        return studentRepositroy.admitedStudentsListRepo(requestDTO.getFromDate().toString(),requestDTO.getToDate().toString())
                .doOnNext(admittedListResponseEntity-> log.info("Successful Admitted Student List Report"))
                .map(m-> new AdmitStudentListResponseDTO(
                        m.getRollno(),
                        m.getFirstname(),
                        m.getLastname(),
                        m.getBranch(),
                        m.getResult(),
                        m.getJoining_date()
                ))
                .doOnNext(dto -> log.info("Fetched Data is :{}",dto));
    }


    public Flux<AdmitStudentListResponseDTO> findStudentByRollNo(AdmitStudentListRequestDTO requestDTO) {


        return studentRepositroy.statusByRollNo(requestDTO.getRollNo())
                .doOnNext(admittedListResponseEntity-> log.info("Roll Number provided for Student Details"))
                .map(m-> new AdmitStudentListResponseDTO(
                        m.getRollno(),
                        m.getFirstname(),
                        m.getLastname(),
                        m.getBranch(),
                        m.getResult(),
                        m.getJoining_date()
                ))
                .doOnNext(dto -> log.info("Fetched Data is :{}",dto));
    }


    private Flux<AdmitStudentListResponseDTO> findStudentByBranch(AdmitStudentListRequestDTO requestDTO) {
        log.info("Requested Branch Name = {}",requestDTO.getBranch());
        return studentRepositroy.findAllByBranch(requestDTO.getBranch().toString())
                .doOnNext(admittedListResponseEntity-> log.info("Branch Name for Student Details"))
                .map(m-> new AdmitStudentListResponseDTO(
                        m.getRollno(),
                        m.getFirstname(),
                        m.getLastname(),
                        m.getBranch(),
                        m.getResult(),
                        m.getJoining_date()
                ))
                .doOnNext(dto -> log.info("Fetched Data is :{}",dto));
    }









    public Map<String, Object> setAdmittedStudentsListParameters(AdmitStudentListRequestDTO requestDTO, Map<String, Object> parameters) {
        parameters.put("fromDate", requestDTO.getFromDate().toString());
        parameters.put("toDate", requestDTO.getToDate().toString());
        parameters.put("title", "Celloscope School");
        Flux<AdmitStudentListResponseDTO> admitStudentListResponseDTOFlux= successfulAdmitStudentList(requestDTO);
        JRBeanCollectionDataSource dataSource = null;

        try {
            dataSource = new JRBeanCollectionDataSource(admitStudentListResponseDTOFlux.collectList().toFuture().get());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        parameters.put("CollectionParam", dataSource);
        return parameters;
    }


    public Map<String, Object> setStudentRollParameters(AdmitStudentListRequestDTO requestDTO, Map<String, Object> parameters) {
        parameters.put("fromDate", requestDTO.getFromDate().toString());
        parameters.put("toDate", requestDTO.getToDate().toString());
        parameters.put("title", "Celloscope School");
        Flux<AdmitStudentListResponseDTO> admitStudentListResponseDTOFlux= findStudentByRollNo(requestDTO);
        JRBeanCollectionDataSource dataSource = null;

        try {
            dataSource = new JRBeanCollectionDataSource(admitStudentListResponseDTOFlux.collectList().toFuture().get());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        parameters.put("CollectionParam", dataSource);
        return parameters;
    }

    public Map<String, Object> setStudentBranchParameters(AdmitStudentListRequestDTO requestDTO, Map<String, Object> parameters) {
        parameters.put("title", "Branch of School");
        Flux<AdmitStudentListResponseDTO> admitStudentListResponseDTOFlux= findStudentByBranch(requestDTO);
        JRBeanCollectionDataSource dataSource = null;

        try {
            dataSource = new JRBeanCollectionDataSource(admitStudentListResponseDTOFlux.collectList().toFuture().get());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        parameters.put("CollectionParam", dataSource);
        return parameters;
    }

}
