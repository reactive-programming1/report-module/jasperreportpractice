package net.celloscope.jasperReportPractice.student.helpers.enums;

import lombok.Getter;

@Getter
public enum QueryParam {
    REQUEST_TYPE("request-type"),

    STATUS("status"),
    SEARCH_TEXT("searchText"),
    FROM_DATE("fromDate"),
    TO_DATE("toDate"),
    OFFSET("offset"),
    LIMIT("limit"),
    BRANCH_ID("branch-id"),
    REPORT_TYPE("report-type"),
    FILE_NAME("fileName"),
    ROLL_NO("rollNo"),
    BRANCH_NAME("branchName")
    ;

    private final String value;

    QueryParam(String value) {
        this.value = value;
    }
}
