package net.celloscope.jasperReportPractice.student.helpers.dataclass.responses;

import lombok.*;
import net.celloscope.jasperReportPractice.common.dataclass.bean.MasterBean;


@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
@Builder
@ToString
public class AdmitStudentListResponseDTO extends MasterBean {
    private int rollno;
    private String firstname;
    private String lastname;
    private String branch;
    private String result;
    private String joining_date;
}
