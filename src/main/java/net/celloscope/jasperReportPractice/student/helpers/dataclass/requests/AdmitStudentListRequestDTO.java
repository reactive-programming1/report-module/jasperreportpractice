package net.celloscope.jasperReportPractice.student.helpers.dataclass.requests;

import lombok.*;
import net.celloscope.jasperReportPractice.common.dataclass.dtos.ServiceRequestDTO;

import java.time.LocalDate;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class AdmitStudentListRequestDTO extends ServiceRequestDTO {
    private LocalDate fromDate;
    private LocalDate toDate;
    private int rollNo;
    private String reportType;
    private String branch;
}
