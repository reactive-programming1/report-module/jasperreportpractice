package net.celloscope.jasperReportPractice.people.services;

import lombok.extern.slf4j.Slf4j;
import net.celloscope.jasperReportPractice.common.service.BaseService;
import net.celloscope.jasperReportPractice.people.database.repositories.PeopleRepository;
import net.celloscope.jasperReportPractice.people.helpers.dataclass.requests.AllPeopleListsRequestDTO;
import net.celloscope.jasperReportPractice.people.helpers.dataclass.responses.AllPeopleListsResponseDTO;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Map;


@Slf4j
@Service
public class PeopleDataService extends BaseService {

    private final PeopleRepository peopleRepository;

    public PeopleDataService(ResourceLoader resourceLoader, PeopleRepository peopleRepository) {
        super(resourceLoader);
        this.peopleRepository = peopleRepository;
    }

    private Flux<AllPeopleListsResponseDTO> findAllPeopleList() {
        return peopleRepository.findAllPeopleData()
                .map(m -> new AllPeopleListsResponseDTO(
                        m.getNid(),
                        m.getPermanent_address_str()
                ));
//                .doOnNext(d -> log.info(" ++++++++ NID = {}  ||  Add = {}",d.getNid(), d.getPermanent_address_str()));
    }

    //Count total people
    private Mono<Long>findPeopleCount(){
        return peopleRepository.findCount().doOnNext(c->log.info("Total Counted People: {}",c));
    }
    
    public Map<String, Object> setPeopleListParameteres(AllPeopleListsRequestDTO allPeopleListsRequestDTO, Map<String, Object> parameters) {
        parameters.put("pTitle", "All People Data");
        Flux<AllPeopleListsResponseDTO> allPeopleListsResponseDTOFlux = findAllPeopleList();
        JRBeanCollectionDataSource dataSource = null;

        // total people value initialized
        Long totalCountPeople=null;

        try {
            //value inserted into a variable
            totalCountPeople = findPeopleCount().toFuture().get();
            dataSource = new JRBeanCollectionDataSource(allPeopleListsResponseDTOFlux.collectList().toFuture().get());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        parameters.put("CollectionBeanParam", dataSource);
        parameters.put("totalCount",totalCountPeople.toString());
        return parameters;
    }
}
