package net.celloscope.jasperReportPractice.people.database.repositories;

import net.celloscope.jasperReportPractice.people.helpers.dataclass.responses.AllPeopleListsResponseDTO;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface PeopleRepository extends ReactiveCrudRepository<AllPeopleListsResponseDTO,String> {

    @Query("SELECT al.nid , vd.permanent_address_str  FROM nda.activity_log al, nda.voter_details vd  where al.nid = vd.national_id")
//    @Query("SELECT al.nid , vd.name_en FROM nda.activity_log al, nda.voter_details vd  where al.nid = vd.national_id ;")
//    @Query("SELECT date_of_birth, national_id  FROM nda.voter_details")
    Flux<AllPeopleListsResponseDTO> findAllPeopleData();

    @Query("SELECT count(*) FROM (SELECT al.nid , vd.permanent_address_str  FROM nda.activity_log al, nda.voter_details vd  where al.nid = vd.national_id) as tmp")
    Mono<Long> findCount();



}
