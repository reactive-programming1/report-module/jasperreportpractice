package net.celloscope.jasperReportPractice.people.helpers.dataclass.requests;

import lombok.*;
import net.celloscope.jasperReportPractice.common.dataclass.dtos.ServiceRequestDTO;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
public class AllPeopleListsRequestDTO extends ServiceRequestDTO {
    private String nid;
}
