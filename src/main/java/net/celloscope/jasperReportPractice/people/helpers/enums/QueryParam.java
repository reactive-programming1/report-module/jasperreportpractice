package net.celloscope.jasperReportPractice.people.helpers.enums;

import lombok.Getter;

@Getter
public enum QueryParam {
    REQUEST_TYPE("request-type"),
    STATUS("status"),
    SEARCH_TEXT("searchText"),
    REPORT_TYPE("report-type"),
    FILE_NAME("fileName"),
    NID_NUMBER("nidNumber")
    ;

    private final String value;

    QueryParam(String value) {
        this.value = value;
    }
}
