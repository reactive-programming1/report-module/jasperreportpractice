package net.celloscope.jasperReportPractice.people.helpers.dataclass.responses;

import lombok.*;
import net.celloscope.jasperReportPractice.common.dataclass.bean.MasterBean;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Builder
@ToString
public class AllPeopleListsResponseDTO extends MasterBean {
    private String nid;
    private String permanent_address_str;
}
