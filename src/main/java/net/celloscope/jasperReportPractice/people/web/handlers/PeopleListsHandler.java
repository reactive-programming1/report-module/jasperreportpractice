package net.celloscope.jasperReportPractice.people.web.handlers;

import lombok.extern.slf4j.Slf4j;
import net.celloscope.jasperReportPractice.common.handler.BaseHandler;
import net.celloscope.jasperReportPractice.common.service.BaseService;
import net.celloscope.jasperReportPractice.people.helpers.dataclass.requests.AllPeopleListsRequestDTO;
import net.celloscope.jasperReportPractice.people.helpers.enums.QueryParam;
import net.celloscope.jasperReportPractice.people.services.PeopleDataService;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

@Slf4j
@Configuration
public class PeopleListsHandler extends BaseHandler {
    private final PeopleDataService service;
    public PeopleListsHandler(PeopleDataService service) {
        super(service);
        this.service=service;
    }

    private AllPeopleListsRequestDTO buildFullListGenerator(ServerRequest request){
        return AllPeopleListsRequestDTO.builder()
                .nid(request.queryParam(QueryParam.NID_NUMBER.getValue()).toString())
                .build();
    }

    public Mono<ServerResponse> fullListGenerator(ServerRequest request) {
        AllPeopleListsRequestDTO allPeopleListsRequestDTO = buildFullListGenerator(request);
        return generateReportResponse(allPeopleListsRequestDTO,"All-People-Data",service::setPeopleListParameteres);
    }
}
