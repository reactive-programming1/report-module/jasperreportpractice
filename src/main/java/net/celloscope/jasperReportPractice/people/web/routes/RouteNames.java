package net.celloscope.jasperReportPractice.people.web.routes;

public class RouteNames {
    public static String BASE_URL = "/people/v1";
    public static String FULL_LIST = "/full-list";
}
