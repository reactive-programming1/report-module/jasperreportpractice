package net.celloscope.jasperReportPractice.people.web.routes;


import lombok.extern.slf4j.Slf4j;
import net.celloscope.jasperReportPractice.people.web.handlers.PeopleListsHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.util.RouteMatcher;
import org.springframework.web.reactive.function.server.RequestPredicates;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;

import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.web.reactive.function.server.RouterFunctions.route;

@Slf4j
@Configuration
public class RouteConfiguration {

    @Bean
    public RouterFunction<ServerResponse> RoutingLists(PeopleListsHandler peopleListsHandler){
        return route()
                .before(this::logRequest)
                .path(RouteNames.BASE_URL,
                        builder -> builder
                                .nest(RequestPredicates.accept(APPLICATION_JSON),
                                        nestBuilder->nestBuilder.GET(RouteNames.FULL_LIST,peopleListsHandler::fullListGenerator))
                        ).after((req, res) -> logResponse(res))
                .build();
    }


    private ServerResponse logResponse(ServerResponse response) {
        log.info("Response sent: {}", response.statusCode());
        return response;
    }

    private ServerRequest logRequest(ServerRequest serverRequest) {
        log.info("Request Received From: {}", serverRequest.uri());
        return serverRequest;
    }
}
