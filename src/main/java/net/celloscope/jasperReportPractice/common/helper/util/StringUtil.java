package net.celloscope.jasperReportPractice.common.helper.util;

public class StringUtil {
    public static boolean isNullOrEmpty(String s) {
        return s == null || s.isEmpty();
    }
}
