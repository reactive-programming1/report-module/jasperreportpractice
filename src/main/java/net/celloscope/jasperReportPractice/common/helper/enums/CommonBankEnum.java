package net.celloscope.jasperReportPractice.common.helper.enums;

import lombok.Getter;

@Getter
public enum CommonBankEnum {
    BANK("Agrani"),
    APP_NAME("Agrani Smart Banking App"),
    BANK_LOGO("Agrani_Bank_SC_logo.png"),
    ;

    private String value;

    CommonBankEnum(String value) {
        this.value = value;
    }
}
