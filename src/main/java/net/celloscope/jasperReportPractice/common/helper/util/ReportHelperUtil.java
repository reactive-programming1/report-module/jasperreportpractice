//package net.celloscope.jasperReportPractice.common.helper.util;
//
//
//import org.jfree.util.StringUtils;
//
//import java.math.BigDecimal;
//import java.text.DecimalFormat;
//import java.text.SimpleDateFormat;
//import java.time.LocalDate;
//import java.time.format.DateTimeFormatter;
//import java.util.Calendar;
//import java.util.Date;
//
//public class ReportHelperUtil {
//
//    public String getReportGenerationDate() {
//        return "তারিখ: " + toBnStr(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime())) + " ইং";
//    }
//
//    public String getCurrentDate() {
//        return toBnStr(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(Calendar.getInstance().getTime())) + " ইং";
//    }
//
//    public String getSimpleDateFormatInBn(Date date) {
//        if(date != null){
//            String pattern = "yyyy-MM-dd";
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//            return toBnStr(simpleDateFormat.format(date)) + " ইং";
//        } else {
//            return StringUtils.EMPTY;
//        }
//
//    }
//
//    public String getSimpleDateFormatInBn(LocalDate date) {
//        if(date != null){
//            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
//            return toBnStr(dateTimeFormatter.format(date)) + " ইং";
//        } else {
//            return StringUtils.EMPTY;
//        }
//
//    }
//
//    public String getSimpleDateFormatByGivenFormat(Date date, String pattern, boolean isBangla) {
//        if(date != null){
//            SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
//            return isBangla? toBnStr(simpleDateFormat.format(date)) : simpleDateFormat.format(date);
//        } else {
//            return StringUtils.EMPTY;
//        }
//
//    }
//
//    public String toBnStr(String s) {
//        return s
//                .replaceAll("0", "০")
//                .replaceAll("1", "১")
//                .replaceAll("2", "২")
//                .replaceAll("3", "৩")
//                .replaceAll("4", "৪")
//                .replaceAll("5", "৫")
//                .replaceAll("6", "৬")
//                .replaceAll("7", "৭")
//                .replaceAll("8", "৮")
//                .replaceAll("9", "৯");
//    }
//
//    @SuppressWarnings("unused")
//    public String getFormattedAmount(BigDecimal value) {
//        if (value == null) { return null; }
//
//        if (value.doubleValue() < 1000) {
//            return toBnStr(format("###.##", value.doubleValue()));
//        } else {
//            double hundreds = value.doubleValue() % 1000;
//            int other = (int) (value.doubleValue() / 1000);
//            return toBnStr(format(",##", other) + ',' + format("000.##", hundreds));
//        }
//
//    }
//
//    private String format(String pattern, Object value) {
//        return new DecimalFormat(pattern).format(value);
//    }
//
//    public boolean nullAndEmptyStringCheck(String str) {
//        boolean response = false;
//        if (str != null && !str.trim().isEmpty()) {
//            response = true;
//        }
//        return response;
//    }
//
//    private static final String[] banglaWordMap = {
//            "",
//            " এক",
//            " দুই",
//            " তিন",
//            " চার",
//            " পাঁচ",
//            " ছয়",
//            " সাত",
//            " আট",
//            " নয়",
//            " দশ",
//            " এগারো",
//            " বারো",
//            " তেরো",
//            " চৌদ্দ",
//            " পনের",
//            " ষোল",
//            " সতের",
//            " আঠার",
//            " উনিশ",
//            " বিশ",
//            " একুশ",
//            " বাইশ",
//            " তেইশ",
//            " চব্বিশ",
//            " পঁচিশ",
//            " ছাব্বিশ",
//            " সাতাশ",
//            " আটাশ",
//            " ঊনত্রিশ",
//            " ত্রিশ",
//            " একত্রিশ",
//            " বত্রিশ",
//            " তেত্রিশ",
//            " চৌত্রিশ",
//            " পঁয়ত্রিশ",
//            " ছত্রিশ",
//            " সাইত্রিশ",
//            " আটত্রিশ",
//            " ঊনচল্লিশ",
//            " চল্লিশ",
//            " একচল্লিশ",
//            " বিয়াল্লিশ",
//            " তেতাল্লিশ",
//            " চুয়াল্লিশ",
//            " পঁয়তাল্লিশ",
//            " ছিচল্লিশ",
//            " সাতচল্লিশ",
//            " আটচল্লিশ",
//            " ঊনপঞ্চাশ",
//            " পঞ্চাশ",
//            " একান্ন",
//            " বাহান্ন",
//            " তেপ্পান্ন",
//            " চুয়ান্ন",
//            " পঞ্চান্ন",
//            " ছাপ্পান্ন",
//            " সাতান্ন",
//            " আটান্ন",
//            " ঊনষাট",
//            " ষাট",
//            " একষট্টি",
//            " বাষট্টি",
//            " তেষট্টি",
//            " চৌষট্টি",
//            " পঁয়ষট্টি",
//            " ছেষট্টি",
//            " সাতষট্টি",
//            " আটষট্টি",
//            " ঊনসত্তুর",
//            " সত্তর",
//            " একাত্তর",
//            " বাহাত্তর",
//            " তেহাত্তুর",
//            " চুয়াত্তর",
//            " পঁচাত্তর",
//            " ছিয়াত্তর",
//            " সাতাত্তর",
//            " আটাত্তর",
//            " ঊনআশি",
//            " আশি",
//            " একাশি",
//            " বিরাশি",
//            " তিরাশি",
//            " চুরাশি",
//            " পঁচাশি",
//            " ছিয়াশি",
//            " সাতাশি",
//            " আটাশি",
//            " ঊননব্বই",
//            " নব্বই",
//            " একানব্বই",
//            " বিরানব্বই",
//            " তিরানব্বই",
//            " চুরানব্বই",
//            " পঁচানব্বই",
//            " ছিয়ানব্বই",
//            " সাতানব্বই",
//            " আটানব্বই",
//            " নিরানব্বই",
//            " শত",
//            " হাজার",
//            " লক্ষ",
//            " কোটি"
//    };
//    private static final String[] multiValues = {
//            "",
//            " হাজার",
//            " লক্ষ",
//            " কোটি",
//            " শত",
//            " হাজার"
//    };
//
//    public String convertAmountToBanglaWordWithCurrency(String number) {
//        return convertAmountToBanglaWord(number)+" টাকা মাত্র";
//    }
//
//    public static String convertAmountToBanglaWord(String amount) {
//        String word = "";
//        String remainder = "";
//        long number = 0L;
//        if (amount.contains(".")) {
//            number = Long.parseLong(amount.split("\\.")[0]);
//            String decimalStr = amount.split("\\.")[1];
//            long decimal = decimalStr.length()==1?Long.parseLong(decimalStr.substring(0,1)):Long.parseLong(decimalStr.substring(0,2));
//            remainder = decimal==0?"":" দশমিক "+convertAmount(decimal);
//        } else {
//            number = Long.parseLong(amount);
//        }
//
//        if (number>999999999) {
//            long firstHalf = (number/1000000000)*100;
//            String firstWord = convertAmount(firstHalf);
//            String secondWord = convertAmount(number%1000000000);
//            if (number%1000000000>9999999) {
//                word = firstWord + secondWord;
//            } else {
//                word = firstWord + " কোটি " + secondWord;
//            }
//        } else {
//            word = convertAmount(number);
//        }
//        if (amount.contains(".")) {
//            word = word + remainder;
//        }
//        return word.replaceAll("\\s+", " ");
//    }
//
//    private static String convertAmount(long number) {
//        if (number==0) return "";
//        String word = "";
//        int index = 0;
//        boolean firstIteration = true;
//        int divisor;
//        do {
//            divisor = firstIteration ? 1000 : 100;
//            // take 3 or 2 digits based on iteration
//            int num = (int)(number % divisor);
//            if (num != 0){
//                String str = ConversionForUptoThreeDigits(num);
//                word = str + multiValues[index] + word;
//            }
//            index++;
//            // next batch of digits
//            number = number/divisor;
//            firstIteration = false;
//        } while (number > 0);
//        return word;
//    }
//
//    private static String ConversionForUptoThreeDigits(int number) {
//        String word = "";
//        int num = number % 100;
//        word = word + banglaWordMap[num];
//        word = (number/100 > 0)? banglaWordMap[number/100] + " শত" + word : word;
//        return word;
//    }
//
//}
