package net.celloscope.jasperReportPractice.common.dataclass.dtos;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@AllArgsConstructor
@Data
public class ServiceResponseDTO {
    private List<?> data;
    private Long totalCount;
}
