package net.celloscope.jasperReportPractice.common.component;

import net.celloscope.jasperReportPractice.common.dataclass.dtos.ServiceRequestDTO;

import java.util.Map;

@FunctionalInterface
public interface ParameterRetrieverComponent<B extends ServiceRequestDTO> {
    Map<String, Object> populateParameters(B requestDTO, Map<String, Object> parameters);
}
