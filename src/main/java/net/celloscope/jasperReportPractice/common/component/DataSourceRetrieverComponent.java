package net.celloscope.jasperReportPractice.common.component;

import net.celloscope.jasperReportPractice.common.dataclass.bean.MasterBean;
import net.celloscope.jasperReportPractice.common.dataclass.dtos.ServiceRequestDTO;
import reactor.core.publisher.Flux;

@FunctionalInterface
public interface DataSourceRetrieverComponent<R extends MasterBean, B extends ServiceRequestDTO> {

    Flux<R> populateJasperReportDataSource(B requestDTO);

}
