package net.celloscope.jasperReportPractice.common.component;

import net.celloscope.jasperReportPractice.common.dataclass.dtos.ServiceRequestDTO;
import reactor.core.publisher.Mono;

@FunctionalInterface
public interface CountRetrieverComponent<B extends ServiceRequestDTO> {
    Mono<Long> populateGridCountDataSource(B requestDTO);
}
