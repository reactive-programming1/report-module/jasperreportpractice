package net.celloscope.jasperReportPractice.common.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.celloscope.jasperReportPractice.common.component.CountRetrieverComponent;
import net.celloscope.jasperReportPractice.common.component.DataSourceRetrieverComponent;
import net.celloscope.jasperReportPractice.common.component.GridDataRetrieverComponent;
import net.celloscope.jasperReportPractice.common.component.ParameterRetrieverComponent;
import net.celloscope.jasperReportPractice.common.dataclass.bean.MasterBean;
import net.celloscope.jasperReportPractice.common.dataclass.dtos.ServiceRequestDTO;
import net.celloscope.jasperReportPractice.common.dataclass.dtos.ServiceResponseDTO;
import net.celloscope.jasperReportPractice.common.service.BaseService;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Slf4j
@RequiredArgsConstructor
public class BaseHandler {

    private final BaseService service;

    protected <R extends MasterBean, B extends ServiceRequestDTO> Mono<ServerResponse> generateReportResponse(
            B requestDTO,
            String reportFileName,
            String reportType,
            DataSourceRetrieverComponent<R, B> dataSourceRetriever,
            ParameterRetrieverComponent<B> parameterRetriever
    ) {
        Map<String, Object> parameters = parameterRetriever.populateParameters(requestDTO, new HashMap<>());
        List<R> responseBean = null;
        try {
            responseBean = dataSourceRetriever.populateJasperReportDataSource(requestDTO).collectList().toFuture().get();
        } catch (Exception e) {
            return Mono.error(e);
        }
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .body(service.exportReport(reportFileName, parameters, responseBean), InputStreamResource.class);
    }

    protected <R extends MasterBean, B extends ServiceRequestDTO> Mono<ServerResponse> generateReportResponse(
            B requestDTO,
            String reportFileName,
            String reportType,
            ParameterRetrieverComponent<B> parameterRetriever
    ) {
        Map<String, Object> parameters = parameterRetriever.populateParameters(requestDTO, new HashMap<>());
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .body(service.exportReport(reportFileName, parameters), InputStreamResource.class);
    }

    // My modified method "generateReportResponse ** No Image Required **"
    protected <R extends MasterBean, B extends ServiceRequestDTO> Mono<ServerResponse> generateReportResponse(
            B requestDTO,
            String reportFileName,
            ParameterRetrieverComponent<B> parameterRetriever
    ) {
        Map<String, Object> parameters = parameterRetriever.populateParameters(requestDTO, new HashMap<>());
        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_PDF)
                .body(service.exportReportNoImage(reportFileName, parameters), InputStreamResource.class);
    }

}
