package net.celloscope.jasperReportPractice.common.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.celloscope.jasperReportPractice.common.dataclass.bean.MasterBean;
import net.celloscope.jasperReportPractice.common.helper.enums.CommonBankEnum;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.util.JRSaver;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.springframework.core.io.InputStreamResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Mono;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Collection;
import java.util.Map;

@Service
@RequiredArgsConstructor
@Slf4j
public class BaseService {

    private final ResourceLoader resourceLoader;

    private static final String PDF_REPORT_PATH = "pdf-templates/";

    public <R extends MasterBean> Mono<InputStreamResource> exportReport(String filename, Map<String, Object> parameters, Collection<R> dataCollections) {
        ;
        Resource imageResources = resourceLoader.getResource("classpath:" + PDF_REPORT_PATH + "images/" + CommonBankEnum.BANK_LOGO.getValue());
        JasperPrint print;
        try {
            parameters.put("imagePathLogo", imageResources.getInputStream());
            JasperReport jasperReport = report(filename);
            print = JasperFillManager.fillReport(jasperReport, parameters, new JRBeanCollectionDataSource(dataCollections));
        } catch (Exception exception) {
            return Mono.error(exception);
        }
        return buildJasperReport(filename, print);
    }

    public <R extends MasterBean> Mono<InputStreamResource> exportReport(String filename, Map<String, Object> parameters) {
        ;
        Resource imageResources = resourceLoader.getResource("classpath:" + PDF_REPORT_PATH + "images/" + CommonBankEnum.BANK_LOGO.getValue());
        JasperPrint print;
        try {
            parameters.put("schollLogo", imageResources.getInputStream());
            log.info("Image File : {}",imageResources.getInputStream());
            JasperReport jasperReport = report(filename);
            print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
        } catch (Exception exception) {
            return Mono.error(exception);
        }
        return buildJasperReport(filename, print);
    }

    // MY CUSTOM BUILD METHOD
    public <R extends MasterBean> Mono<InputStreamResource> exportReportNoImage(String filename, Map<String, Object> parameters) {
        JasperPrint print;
        try {
            JasperReport jasperReport = report(filename);
            print = JasperFillManager.fillReport(jasperReport, parameters, new JREmptyDataSource());
        } catch (Exception exception) {
            return Mono.error(exception);
        }
        return buildJasperReport(filename, print);
    }




    Mono<InputStreamResource> buildJasperReport(String filename, JasperPrint print) {
        JRPdfExporter exporter = new JRPdfExporter();
        SimpleOutputStreamExporterOutput simpleExporterOutput = new SimpleOutputStreamExporterOutput(filename + ".pdf");
        exporter.setExporterInput(new SimpleExporterInput(print));
        exporter.setExporterOutput(simpleExporterOutput);
        FileInputStream st;
        try {
            exporter.exportReport();
            st = new FileInputStream(filename + ".pdf");
        } catch (Exception exception) {
            return Mono.error(exception);
        }
        return Mono.just(new InputStreamResource(st));
    }

    JasperReport report(String filename) throws JRException, IOException {
        JasperReport jr = null;
        File f = new File(filename + ".jasper");
        if (f.exists()) {
//            jr = (JasperReport) JRLoader.loadObject(f);
            Resource resource = resourceLoader.getResource("classpath:" + PDF_REPORT_PATH + filename + ".jrxml");
            jr = JasperCompileManager.compileReport(resource.getInputStream());
            JRSaver.saveObject(jr, filename + ".jasper");
        } else {
            Resource resource = resourceLoader.getResource("classpath:" + PDF_REPORT_PATH + filename + ".jrxml");
            jr = JasperCompileManager.compileReport(resource.getInputStream());
            JRSaver.saveObject(jr, filename + ".jasper");
        }
        return jr;
    }

}
