package net.celloscope.jasperReportPractice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JasperReportPracticeApplication {

	public static void main(String[] args) {
		SpringApplication.run(JasperReportPracticeApplication.class, args);
	}

}
